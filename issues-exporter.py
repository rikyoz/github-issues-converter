#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Converts GitHub issues in a format importable by Bitbucket.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import argparse
import json
import requests
import re
from zipfile import ZipFile

__author__ = "rikyoz"
__copyright__ = "Copyright 2015 rikyoz"
__license__ = "GNU GPL v2"
__version__ = "0.1"
__maintainer__ = "rikyoz"
__email__ = "rik20@live.it"
__status__ = "Production"

def RepositoryString(repo):
    if re.compile("[\w\d]+/[\w\d]+").match(repo):
        return repo
    else:
        raise argparse.ArgumentTypeError("Repository must follow the format username/repository")

def parse_arguments():
    parser = argparse.ArgumentParser(description="Converts GitHub issues in a format importable by Bitbucket. It calls"
                                                 " the GitHub Issue APIs, requesting all the issues of the target"
                                                 " repository. Then, it converts them into the"
                                                 " 'Import/Export Data Format' used by Bitbucket"
                                                 " repositories (see https://goo.gl/OCH2qg)")
    parser.add_argument("repository", type=RepositoryString, metavar="repository", nargs=1, action="store",
                        help="the target repository on github (format: username/repository)")
    parser.add_argument("-u", "--user", action="store", default="",
                        help="github username (needed if the repository is private)")
    parser.add_argument("-p", "--password", action="store", default="",
                        help="github password (needed if the repository is private)")
    parser.add_argument("-o", "--output", action="store", default="issues",
                        help="output zip file name (default: 'issues')")
    return parser.parse_args()

def github_request(url, user = "", password = ""):
    if not user or not password:
        return requests.get(url).json()
    else:
        return requests.get(url, auth=(user, password)).json()


def main():
    args = parse_arguments()

    github_api = "https://api.github.com/repos/"
    issues_api = github_api + args.repository[0] + "/issues?state=all&filter=all&per_page=100"
    milestones_api = github_api + args.repository[0] + "/milestones?state=all"

    print "Requesting data from GitHub repository...",

    github_issues = github_request(issues_api, args.user, args.password)
    github_milestones = github_request(milestones_api, args.user, args.password)

    if "message" in github_issues:
        print "error"
        if github_issues["message"] == "Not Found":
            print "\tRepository not found, check if it exists!"
            print "\tNote: If it does exist, but it is private and you have read access,\n" \
                  "\t      call this script specifying your github username and password"
        else:
            print "\t" + github_issues["message"]
        exit(1)

    print "done"
    print "Parsing data...",

    bitbucket = {"issues": [], "milestones": []}

    for issue in github_issues:
        bitbucket_issue = {}

        if "assignee" in issue:
            bitbucket_issue["assignee"] = str(issue["assignee"]["login"])

        bitbucket_issue["component"] = None
        bitbucket_issue["content"] = str(issue["body"])
        bitbucket_issue["content_updated_on"] = str(issue["updated_at"])
        bitbucket_issue["created_on"] = str(issue["created_at"])
        bitbucket_issue["edited_on"] = None
        bitbucket_issue["id"] = issue["number"]

        bitbucket_issue["kind"] = "bug"
        bitbucket_issue["status"] = str(issue["state"])

        # Note: bitbucket issues have a single "kind" attribute while github issues can have
        #       multiple "labels" (not necessarily equivalent to the "kind" values), so only one
        #       github label will be used as the "kind" attribute of the equivalent bitbucket
        #       issue. The same applies to the "status" attribute of bitbucket issues.
        for label in issue["labels"]:
            if label["name"] in ("bug", "enhancement", "proposal", "task"):
                bitbucket_issue["kind"] = label["name"]
            elif label["name"] in ("duplicate", "invalid", "wontfix"):
                bitbucket_issue["status"] = label["name"]

        bitbucket_issue["priority"] = str("major")
        bitbucket_issue["reporter"] = str(issue["user"]["login"])
        bitbucket_issue["milestone"] = str(issue["milestone"]["title"])
        bitbucket_issue["title"] = str(issue["title"])
        bitbucket_issue["updated_on"] = str(issue["updated_at"])
        bitbucket_issue["version"] = None
        bitbucket_issue["watchers"] = []
        bitbucket_issue["voters"] = []
        bitbucket["issues"].append(bitbucket_issue)

    for milestone in github_milestones:
        bitbucket["milestones"].append({"name": str(milestone["title"])})

    bitbucket["meta"] = {"default_kind": "bug"}

    with open("db-1.0.json", "w") as outfile:
        json.dump(bitbucket, outfile, indent=4, sort_keys=True)

    with ZipFile(args.output + ".zip", "w") as zipf:
        zipf.write("db-1.0.json")

    print "done"
    print "Export completed"

if __name__ == '__main__':
   main()