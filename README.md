# GitHub to Bitbucket Issues Converter

A script to convert GitHub issues in a format importable by Bitbucket.

![](http://img.shields.io/badge/version-v0.1.0-red.svg?style=flat) ![](https://img.shields.io/badge/python-2.7.x-yellow.svg?style=flat) ![](http://img.shields.io/badge/license-GPL%20v2-blue.svg?style=flat) 

## How does it works?

This script calls the GitHub Issue APIs, requesting all the issues of the target repository.
Then, it converts the result into a json file which follows the 'Import/Export Data Format' used by Bitbucket's repositories (see https://goo.gl/OCH2qg). In the end, it creates a zip file containing the converted file.

## Requirements

+ `Python 2.7.x`
+ `requests` for Python (see http://docs.python-requests.org/en/latest/user/install/#install)

## Usage

	> python issues-exporter.py repository [-u USER] [-p PASSWORD] [-o OUTPUT] [-h]

### Required arguments

	repository                 the target repository on github (format: username/repository)
	
### Optional arguments

	-h, --help                              show a help message and exit
	-u USER, --user USER                    github username (needed if the repository is private)
	-p PASSWORD, --password PASSWORD        github password (needed if the repository is private)
    -o OUTPUT, --output OUTPUT              output zip file name (default: 'issues')